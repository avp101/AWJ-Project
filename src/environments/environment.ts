// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export let environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC2j3m60knjl7JK7xLw5KQhm5Ek2drAeOE',
    authDomain: 'awj-project.firebaseapp.com',
    databaseURL: 'https://awj-project.firebaseio.com',
    projectId: 'awj-project',
    storageBucket: 'awj-project.appspot.com',
    messagingSenderId: '228170655131'
  }
};
