import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register//register.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { UserComponent } from './user/user.component';
import { NotAuthGuard } from './guards/not-auth.guard';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [NotAuthGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [NotAuthGuard]},
  {path: 'attendance', component: AttendanceComponent, canActivate: [AuthGuard]},
  {path: 'user', component: UserComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/attendance', pathMatch: 'full' },
  { path: '**', redirectTo: '/attendance' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
