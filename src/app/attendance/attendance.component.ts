import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AttendanceService } from '../services/attendance.service';
import { Course } from '../classes/Course';
import { Laboratory } from '../classes/Laboratory';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  activeCourse: Course;
  activeLaboratory: Laboratory;

  constructor(private _attendanceService: AttendanceService) { }
  ngOnInit() {
    this._attendanceService.getCourses().subscribe(result => this.getCurrentCourse(result));
    this._attendanceService.getLaboratories().subscribe(result => this.getCurrentLaboratory(result));
  }

  getCurrentCourse(result): void {
    for (let i = 0; i < result.length; i++) {
      if (this._attendanceService.IsCourseHappening(result[i])) {
        this.activeCourse = new Course();
        this.activeCourse.id = result[i]['id'];
        this.activeCourse.name = result[i]['name'];
        this.activeCourse.groups = [];
        for (const item of result[i]['groups']) { item != null ? this.activeCourse.groups.push(item) : console.log(''); }
        this.activeCourse.dates = [];
        for (const item of result[i]['dates']) {
          if (item != null) {
            this.activeCourse.dates.push({'day': item['day'], 'hour': item['hour']} );
          }
        }
        break;
      }
    }
  }

  getCurrentLaboratory(result): void {
    for (let i = 0; i < result.length; i++) {
      if (this._attendanceService.IsLaboratoryHappening(result[i])) {
        this.activeLaboratory = new Laboratory();
        this.activeLaboratory.id = result[i]['id'];
        this.activeLaboratory.name = result[i]['name'];
        this.activeLaboratory.groups = [];
        for (const item of result[i]['groups']) { item != null ? this.activeLaboratory.groups.push(item) : console.log(''); }
        this.activeLaboratory.dates = [];
        for (const item of result[i]['dates']) {
          if (item != null) {
            this.activeLaboratory.dates.push({'day': item['day'], 'hour': item['hour']} );
          }
        }
        break;
      }
    }
  }

  attend(value): void {
    this._attendanceService.Attend(value);
  }

}
