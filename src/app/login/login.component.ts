import { Component, OnInit } from '@angular/core';
import { User } from '../classes/User';
import { AuthServiceService } from '../services/auth-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any = {
    username: '',
    password: ''
  };
  constructor(private _authService: AuthServiceService, private _router: Router) {}

  ngOnInit() {}

  async loginUser() {
     await this._authService.Login(this.user.username, this.user.password);
     this._router.navigate(['/attendance']);
  }
}
