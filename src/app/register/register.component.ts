import { Component, OnInit } from '@angular/core';
import {User} from '../classes/User';
import { AuthServiceService } from '../services/auth-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  constructor(private _authService: AuthServiceService, private _router: Router) {
    this.user = new User();
   }

  ngOnInit() {
  }

  async registerUser() {
    await this._authService.Register(this.user);
    this._router.navigate(['/login']);
  }
}
