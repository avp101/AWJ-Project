import {Date} from './Date';

export class Laboratory {
  name: string;
  dates: Date[];
  groups: string[];
  id: number;
}
