import {Date} from './Date';
export class Course {
  name: string;
  dates: Date[];
  groups: string[];
  id: number;
}
