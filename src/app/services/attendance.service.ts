import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserService } from './user.service';

@Injectable()
export class AttendanceService {

  private weekDays = {
    'Monday': 1,
    'Tuesday': 2,
    'Wednesday': 3,
    'Thursday': 4,
    'Friday': 5,
    'Saturday': 6,
    'Sunday': 7
  };
  constructor(private _db: AngularFireDatabase, private _userService: UserService) { }

  public getCourses() {
      return this._db.list('/courses').valueChanges();
  }

  public getLaboratories() {
    return this._db.list('/laboratories').valueChanges();
  }

  public IsCourseHappening(value): boolean {
    const user = this._userService.getCurrentUser();
    const currentDate = new Date();
    if (value.groups.indexOf(user.id) < 0) {
      return false;
    }
    for (const item of value.dates) {
      if (item != null) {
        if ((currentDate.getDay() === this.weekDays[item.day])
        && (item.hour <= currentDate.getHours() * 100)
        &&  (currentDate.getHours() * 100 <= (item.hour + 200))) {
          return true;
        }
      }
    }
    return false;
  }
  public IsLaboratoryHappening(value): boolean {
    const user = this._userService.getCurrentUser();
    const currentDate = new Date();
    if (value.groups.indexOf(user.id) < 0) {
      return false;
    }
    for (const item of value.dates) {
      if (item != null) {
        if ((currentDate.getDay() === this.weekDays[item.day])
        && (item.hour <= currentDate.getHours() * 100)
        &&  (currentDate.getHours() * 100 <= (item.hour + 200))) {
          return true;
        }
      }
    }
    return false;
  }

  public async Attend(value) {
    const user = this._userService.getCurrentUser();
    user.attendance = value.id;
    this._userService.setCurrentUser(user);
    this._db.object('users/' + this._userService.getUserId()).update(user);
  }

}
