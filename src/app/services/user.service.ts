import { Injectable } from '@angular/core';
import {User} from '../classes/User';
@Injectable()
export class UserService {

  private _currentUser: User = new User();
  private _userId: number;
  constructor() { }

  public getCurrentUser(): User {
    return this._currentUser;
  }
  public setCurrentUser(usr): void {
    this._currentUser.username = usr.username;
    this._currentUser.email = usr.email;
    this._currentUser.name = usr.name;
    this._currentUser.attendance = usr.attendance;
    this._currentUser.id = usr.id;
    this._currentUser.surname = usr.surname;
  }
  public setUserId(value): void {
    this._userId = value + 1;
  }
  public getUserId(): number {
    return this._userId;
  }


}
