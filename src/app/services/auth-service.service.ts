import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserService } from './user.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthServiceService {

  private _isAuth = false;
  constructor(private _db: AngularFireDatabase, private _userService: UserService, private _router: Router) { }

  public async Login(username: string, password: string): Promise<any> {
    return new Promise(resolve => {
      this._db.list('/users').valueChanges().subscribe(result => {
        for (let i = 0; i < result.length; i++) {
            if (result[i]['username'] === username && result[i]['password'] === password) {
                this._userService.setCurrentUser(result[i]);
                this._userService.setUserId(i);
                this._isAuth = true;
                resolve(true);
                break;
              }
        }
        resolve(false);
      });
    });
  }

  public async Register(data): Promise<any> {
      const subscribtion = this._db.list('/users').valueChanges().subscribe(result => {
          subscribtion.unsubscribe();
          this._db.object('/users/' + (result.length + 1)).set(data);
         });
  }


  public Logout(): void {
    this._isAuth = false;
    this._router.navigate(['/login']);
  }

  public IsAuthentiated() {
    return this._isAuth;
  }

}
