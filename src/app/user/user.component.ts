import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {User} from '../classes/User';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;
  constructor(private _userService: UserService) { }

  ngOnInit() {
    this.user = this._userService.getCurrentUser();
  }

}
