import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';

@Injectable()
export class NotAuthGuard implements CanActivate {
  constructor(private _authService: AuthServiceService, private _router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(!this._authService.IsAuthentiated()) {
        return true;
      }
    this._router.navigate(['']);
    return false;
  }
}
